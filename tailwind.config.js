module.exports = {
  purge: [],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [
    function ({ addBase, config }) {
      addBase({
        'body': { backgroundColor: '#e2e8f0' },
      })
    }],
}
