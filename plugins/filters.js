import Vue from 'vue'

Vue.filter('toJune', function (value) {
  if (typeof value !== "number") {
      return value;
  }
  let val = (value/1).toFixed(2).replace('.', ',')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " Ǧ1"
});