export default {
  buildModules: ['@nuxtjs/tailwindcss'],
  plugins: ['~plugins/filters.js'],
  components: true
}
